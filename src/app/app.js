import React, { Component } from 'react';

import Snake from './snake';
import Food from './food';

import './app.css';

import logo from './images/caduceus.svg';
import qweb from './images/qweb.svg';

const getRandomCoordinates = () => {
  let min = 1;
  let max = 98;
  let x = Math.floor((Math.random()*(max-min+1)+min)/2)*2;
  let y =  Math.floor((Math.random()*(max-min+1)+min)/2)*2;
  return [x,y]
}

var playerName = "Player";

const sidebarTitle = "High Scores";
const gameCoreTitle = "Welcome to the React Tutorial!";

const HighScoreData = React.createContext();

class App extends Component {
	constructor(props) {
    super(props);
    this.state = {
      data: [[playerName,0],[playerName,0],[playerName,0],[playerName,0],[playerName,0],[playerName,0],[playerName,0],[playerName,0],[playerName,0],[playerName,0]]
    };
  }
	
	setDataState = (val) => {
		var tempScores = this.state.data
		for (var i = 0; i < 10; i++) {
			if (val > tempScores[i][1]) {
				tempScores.splice(i, 0, [playerName,val]); 
				tempScores.pop();
				break;
			}
		}
		this.setState({data: tempScores})
	}
	
  render() {
    return (
			<div className="app">
				<div className="content flex-row">
					<HighScoreData.Provider value={this.state.data}>
						<Sidebar title={sidebarTitle}/>
					</HighScoreData.Provider>
					<GameCore title={gameCoreTitle} dataStateCallback={this.setDataState}/>
				</div>
			</div>
		);
  }
}

class Sidebar extends Component {
	render() {
    return (
			<div className="sidebar flex-column">
				<div className="header-sidebar flex-row">
					<img className="logo" src={logo} alt="logo"/>
					<span className="title">{this.props.title}</span>
				</div>
				<div className="card flex-column">
					<div className="data">
						<HighScoresTable/>
					</div>
				</div>
			</div>
		);
	}
}

class HighScoresTable extends Component {
	static contextType = HighScoreData;
	populateTable = () => {
		var rows = [];
		for (var i = 0; i < 11; i++) {
			if (i === 0) {
				rows.push(<TableHeaderRow key={"thr"+i.toString()} keyValue={i}/>);
			} else {
				if (this.context[i-1][1] === 0) {
					rows.push(<TableDataRow key={"thr"+i.toString()} rank={i} name={""} score={""} keyValue={i}/>);
				} else {
					rows.push(<TableDataRow key={"thr"+i.toString()} rank={i} name={this.context[i-1][0]} score={this.context[i-1][1]} keyValue={i}/>);
				}
			}
		}
		return rows
	}
	
	render() {		
    return (
			<table>
				<tbody>
					{this.populateTable()}
				</tbody>
			</table>
		);
	}
}

class TableHeaderRow extends Component {
	render() {
    return (
			<tr key={"tr"+(this.props.keyValue).toString()}>
				<th key={"th1"}>Rank</th>
				<th key={"th2"}>Name</th>
				<th key={"th3"}>Score</th>
			</tr>
		);
	}
}

class TableDataRow extends Component {
	render() {
    return (
			<tr key={"tr"+(this.props.keyValue).toString()}>
				<td key={"td1"}>{this.props.rank}.</td>
				<td key={"td2"}>{this.props.name}</td>
				<td key={"td3"}>{this.props.score}</td>
			</tr>
		);
	}
}

class GameCore extends Component {
	constructor(props) {
    super(props);
    this.state = {
      display: 1,
			score: 0
    };
  }
	
	setDisplayState = (val) => {
		this.setState({display: val})
	}
	
	setScoreState = (val) => {
		this.setState({score: val})
	}
	
	getDisplayElements = (displayStateCallback,scoreStateCallback) => {
		if (this.state.display === 1) {
			return (
				<Menu displayStateCallback={displayStateCallback}/>
			);
		} else if (this.state.display === 2) {
			return (
				<Caduceus displayStateCallback={displayStateCallback} scoreStateCallback={scoreStateCallback} dataStateCallback={this.props.dataStateCallback}/>
			);
		} else if (this.state.display === 3) {
			return (
				<Credits displayStateCallback={displayStateCallback}/>
			);
		} else if (this.state.display === 4) {
			return (
				<GameOver displayStateCallback={displayStateCallback} finalScore={this.state.score}/>
			);
		} else if (this.state.display === 4) {
			return (
				<GameOver displayStateCallback={displayStateCallback} finalScore={this.state.score}/>
			);
		} else if (this.state.display === 5) {
			return (
				<Settings displayStateCallback={displayStateCallback}/>
			);
		}
	}
	
	render() {
    return (
			<div id="core" className="core flex-column">
				<Background/>
				<Header title={this.props.title}/>
				<div className="game-area flex-column">
					<div className="play-area">
						<div className="game-container flex-column">
							{this.getDisplayElements(this.setDisplayState,this.setScoreState)}
						</div>
					</div>
				</div>
			</div>
		);
	}
}

class Background extends Component {
	render() {
    return (
			<div id="canvas-container"></div>
		);
	}
}

class Header extends Component {
	render() {
    return (
			<div className="header-core flex-row">
				<span className="text">{this.props.title}</span>
			</div>
		);
	}
}

class Menu extends Component {
	render() {
    return (
			<React.Fragment>
				<button className="button" type="button" onClick={() => this.props.displayStateCallback(2)}>
					<span>Play Game</span>
				</button>
				<button className="button" type="button" onClick={() => this.props.displayStateCallback(5)}>
					<span>Settings</span>
				</button>
				<button className="button" type="button" onClick={() => this.props.displayStateCallback(3)}>
					<span>Credits</span>
				</button>
			</React.Fragment>
		);
	}
}

class Settings extends Component {
	updatePlayerName = () => {
		playerName = document.getElementById('input-name').value
		this.props.displayStateCallback(1)
	}
	
	render() {
    return (
			<React.Fragment>
				<div className="settings-header flex-column">
					<span className="settings-text">Name:</span>
					<form className="form">
						<input id="input-name" type="text" name="player-name" placeholder="Enter Your Name..."/><br/>
					</form>
					<button className="button" type="button" onClick={() => this.updatePlayerName()}>
						<span>Submit</span>
					</button>
				</div>
			</React.Fragment>
		);
	}
}

class Credits extends Component {
	render() {
    return (
			<img className="logo-qweb" src={qweb} alt="qweb" onClick={() => this.props.displayStateCallback(1)}/>
		);
	}
}

class GameOver extends Component {
	render() {
    return (
			<React.Fragment>
				<div className="gameover-header flex-column">
					<span className="gameover-text">Game Over!</span>
					<span className="gameover-score">Final Score: {this.props.finalScore}</span>
				</div>
				<button className="button" type="button" onClick={() => this.props.displayStateCallback(2)}>
					<span>Play Again</span>
				</button>
				<button className="button" type="button" onClick={() => this.props.displayStateCallback(1)}>
					<span>Return to Menu</span>
				</button>
			</React.Fragment>
		);
	}
}

class Caduceus extends Component {
	constructor(props) {
    super(props);
    this.state = {
			food: getRandomCoordinates(),
			interval: null,
			speed: 100,
			direction: 'RIGHT',
			snakeDots: [
				[0,0],
				[2,0]
			]
		};
  }

  componentDidMount() {
		var interval = setInterval(this.moveSnake, this.state.speed);
    document.onkeydown = this.onKeyDown;
		this.setState({interval: interval});
  }

  componentDidUpdate() {
    this.checkIfOutOfBorders();
    this.checkIfCollapsed();
    this.checkIfEat();
  }
	
	componentWillUnmount() {
		clearInterval(this.state.interval);
	}

  onKeyDown = (e) => {
    e = e || window.event;
    switch (e.keyCode) {
      case 38:
        this.setState({direction: 'UP'});
        break;
      case 40:
        this.setState({direction: 'DOWN'});
        break;
      case 37:
        this.setState({direction: 'LEFT'});
        break;
      case 39:
        this.setState({direction: 'RIGHT'});
        break;
			default:
				break;
    }
  }

  moveSnake = () => {
    let dots = [...this.state.snakeDots];
    let head = dots[dots.length - 1];

    switch (this.state.direction) {
      case 'RIGHT':
        head = [head[0] + 2, head[1]];
        break;
      case 'LEFT':
        head = [head[0] - 2, head[1]];
        break;
      case 'DOWN':
        head = [head[0], head[1] + 2];
        break;
      case 'UP':
        head = [head[0], head[1] - 2];
        break;
			default:
				break;
    }
    dots.push(head);
    dots.shift();
    this.setState({
      snakeDots: dots
    })
  }

  checkIfOutOfBorders() {
    let head = this.state.snakeDots[this.state.snakeDots.length - 1];
    if (head[0] >= 100 || head[1] >= 100 || head[0] < 0 || head[1] < 0) {
      this.onGameOver();
    }
  }

  checkIfCollapsed() {
    let snake = [...this.state.snakeDots];
    let head = snake[snake.length - 1];
    snake.pop();
    snake.forEach(dot => {
      if (head[0] === dot[0] && head[1] === dot[1]) {
        this.onGameOver();
      }
    })
  }

  checkIfEat() {
    let head = this.state.snakeDots[this.state.snakeDots.length - 1];
    let food = this.state.food;
    if (head[0] === food[0] && head[1] === food[1]) {
      this.setState({
        food: getRandomCoordinates()
      })
      this.enlargeSnake();
      this.increaseSpeed();
    }
  }

  enlargeSnake() {
    let newSnake = [...this.state.snakeDots];
    newSnake.unshift([])
    this.setState({
      snakeDots: newSnake
    })
  }

  increaseSpeed() {
    if (this.state.speed > 10) {
			var newSpeed = this.state.speed - 10
			clearInterval(this.state.interval);
			var interval = setInterval(this.moveSnake, newSpeed);
			var newState = {
				speed: newSpeed,
				interval: interval
			};
			this.setState(newState)
    }
  }

  onGameOver() {
    // alert(`Game Over. Snake length is ${this.state.snakeDots.length}`);
		this.props.scoreStateCallback(this.state.snakeDots.length);
		this.props.dataStateCallback(this.state.snakeDots.length);
		this.props.displayStateCallback(4);
	}
	
	render() {
    return (
			<React.Fragment>
				<Snake snakeDots={this.state.snakeDots}/>
				<Food dot={this.state.food}/>
			</React.Fragment>
		);
	}
}

export default App;
